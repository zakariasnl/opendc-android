package com.opendc.opendc

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ListView
import com.opendc.opendc.datacenter.CreateDatacenter
import com.opendc.opendc.db.DatacenterDB
import com.opendc.opendc.odcAndroid.jpa.schema.Datacenter
import com.opendc.opendc.simulations.SimulationActivity
import java.util.logging.Logger


class MainActivity : AppCompatActivity() {
    private lateinit var listView: ListView
    private lateinit var datacenterDB: DatacenterDB
    private var selectedItem: Datacenter? = null

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(MainActivity::class.java.name)
        val SELECTED_DATACENTER_WITH_ID = "selected_datacenter"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        datacenterDB = DatacenterDB(this)

        listView = findViewById(R.id.datacenterListView)

        val adapter = DatacenterListAdapter(this, datacenterDB.readAllDatacenters())
        listView.adapter = adapter

        // Add click listener for items
        listView.setOnItemClickListener { _, _, position, id ->
            LOG?.info("Selected datacenter item: ${position}")

            selectedItem = adapter.getItem(position) as Datacenter
        }
    }

    fun createDatacenter(view: View) {
        val intent = Intent(this, CreateDatacenter::class.java)
        startActivity(intent)
    }

    fun runExperiment(view: View) {
        if (selectedItem != null) {
            val intent = Intent(this, SimulationActivity::class.java)
            intent.putExtra(SELECTED_DATACENTER_WITH_ID, selectedItem?.id)
            startActivity(intent)
        } else {
            LOG?.info("No datacenter selected")
        }
    }
}
