package com.opendc.opendc.experiments

import android.app.Activity
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.atlarge.opendc.model.odc.platform.workload.Task
import com.atlarge.opendc.model.topology.destinations
import com.atlarge.opendc.simulator.Duration
import com.atlarge.opendc.simulator.kernel.Kernel
import com.atlarge.opendc.model.odc.platform.workload.TaskState
import com.atlarge.opendc.simulator.platform.Experiment
import com.opendc.opendc.R
import com.opendc.opendc.odcAndroid.JpaBootstrap
import com.opendc.opendc.odcAndroid.jpa.schema.*
import org.w3c.dom.Text
import com.opendc.opendc.odcAndroid.jpa.schema.Experiment as InternalExperiment
import com.opendc.opendc.odcAndroid.jpa.schema.Task as InternalTask
import com.opendc.opendc.odcAndroid.jpa.schema.TaskState as InternalTaskState
import com.opendc.opendc.odcAndroid.jpa.schema.Trace as InternalTrace
import java.io.Closeable
import java.util.logging.Logger


/**
 * An [Experiment] executable by [executorService], directed for
 * running on Android devices.
 *
 * @param factory The simulation kernel implementation to use.
 * @param timeout The maximum duration of the experiment before returning to the caller.
 * @author: Zakarias Nordfäldt-Laws
 */

class JpaExperimentAndroid(private var experiment: InternalExperiment,
                           val textView: TextView,
                           val spinner: ProgressBar,
                           val activity: Activity) : Experiment<Unit>, Closeable {

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(JpaExperimentAndroid::class.java.name)
    }


    /**
     * Perform one entire execution of an experiment, output is logged
     *
     * @return
     *   null: If the execution has reached the [timeout]
     *   Unit: Upon successfully completing the experiment
     */
    override fun run(factory: Kernel, timeout: Duration): Unit? {
        if (experiment.state != ExperimentState.CLAIMED) {
            throw IllegalStateException("The experiment is in illegal state ${experiment.state}")
        }

        // Set the simulation state
        experiment.state = ExperimentState.SIMULATING

        val bootstrap = JpaBootstrap(experiment)
        val simulation = factory.create(bootstrap)
        val topology = simulation.model

        val section = experiment.path.sections.first()
        val trace = experiment.trace
        val tasks = trace.jobs.flatMap { it.tasks }

        // Find all machines in the datacenter
        val machines = topology.run {
            section.datacenter.outgoingEdges.destinations<Room>("room").asSequence()
                    .flatMap { it.outgoingEdges.destinations<Rack>("rack").asSequence() }
                    .flatMap { it.outgoingEdges.destinations<Machine>("machine").asSequence() }.toList()
        }

        LOG?.info ( "Starting simulation" )

        // Run untill all jobs have been finished
        while (trace.jobs.any { !it.finished }) {
            // If we have reached a timeout, return
            if (simulation.time >= timeout)
                return null

            // Collect data of simulation cycle
            experiment.last = simulation.time

            machines.forEach { machine ->
                val state = simulation.run { machine.state }
                val wrapped = MachineState(0,
                        machine,
                        state.task as com.opendc.opendc.odcAndroid.jpa.schema.Task?,
                        experiment,
                        simulation.time,
                        state.temperature,
                        state.memory,
                        state.load
                )
                updateUiMachineState(wrapped)
            }

            tasks.forEach { task ->
                val state = InternalTaskState(0,
                        task as com.opendc.opendc.odcAndroid.jpa.schema.Task,
                        experiment,
                        simulation.time,
                        task.remaining.toInt(),
                        1
                )
                //updateUiMachineTaskState(state)
            }

            // Run next simulation cycle
            simulation.step()
        }

        finishExperiment(tasks)

        return Unit
    }

    fun finishExperiment(tasks: List<Task>){
        // Set the experiment state
        experiment.state = ExperimentState.FINISHED


        LOG?.info("Simulation done" )
        val waiting: Long = tasks.fold(0.toLong()) { acc, task ->
            val finished = task.state as TaskState.Finished
            acc + (finished.previous.at - finished.previous.previous.at)
        } / tasks.size

        val execution: Long = tasks.fold(0.toLong()) { acc, task ->
            val finished = task.state as TaskState.Finished
            acc + (finished.at - finished.previous.at)
        } / tasks.size

        val turnaround: Long = tasks.fold(0.toLong()) { acc, task ->
            val finished = task.state as TaskState.Finished
            acc + (finished.at - finished.previous.previous.at)
        } / tasks.size

        LOG?.info("Average waiting time: $waiting seconds" )
        LOG?.info("Average execution time: $execution seconds" )
        LOG?.info("Average turnaround time: $turnaround seconds" )

        //Remove spinner
        spinner.visibility = View.GONE

        textView.post {
            val text = "DONE SIMULATING\n\n" +
                    "Average waiting time: $waiting seconds\n" +
                    "Average execution time: $execution seconds\n" +
                    "Average turnaround time: $turnaround seconds\n\n" + textView.text.toString()

            textView.text = text

        }
    }

    override fun run(factory: Kernel) {
        LOG?.info("Running infinite simulation")
        run(factory, -1)
    }

    override fun close() {
        return
    }

    private fun updateUiMachineState(machineState: MachineState) {
        val machineView: LinearLayout = activity.findViewById(machineState.machine.id)

        machineView.post {
            val wrapper = machineView.getChildAt(1) as LinearLayout

            // get temperature TextView
            val tempWrapper = wrapper.getChildAt(1) as LinearLayout
            val tempView = tempWrapper.getChildAt(1) as TextView
            tempView.text = machineState.temperature.toString()

            // get memory TextView
            val memWrapper = wrapper.getChildAt(1) as LinearLayout
            val memoryView = memWrapper.getChildAt(1) as TextView
            memoryView.text = machineState.memoryUsage.toString()

            // get load TextView
            val loadWrapper = wrapper.getChildAt(1) as LinearLayout
            val loadView = loadWrapper.getChildAt(1) as TextView
            loadView.text = machineState.load.toString()

        }
//        textView.post {
//            textView.append("------Machine ${machineState.machine.id} time ${machineState.time}-----\n" +
//                    "Temperature: ${machineState.temperature}\n" +
//                    "Memory usage: ${machineState.memoryUsage}\n" +
//                    "Work load: ${machineState.load}\n\n")
//
//        }
    }

    private fun updateUiMachineTaskState(taskState: InternalTaskState) {
        textView.post {
            textView.text = "------Task ${taskState.id} time ${taskState.time}-----\n" +
                    "Remaining: ${taskState.remaining}\n"
        }
    }

}
