package com.opendc.opendc.db

import android.provider.BaseColumns

/**
 * @author: Zakarias Nordfäldt-Laws
 */

val DATABASE_NAME = "datacenter.db"
val DATABASE_VERSION = 10

object DatacenterTable : BaseColumns {
    val ID = "id"
    val TABLE_NAME = "datacenter_table"
    val NAME_COL = "name"
}

object RoomTable : BaseColumns {
    val ID = "id"
    val TABLE_NAME = "room_table"
    val NAME_COL = "name"
    val TYPE_COL = "room_type"
    val DATACENTER_ID = "datacenter_id"
}

object RackTable : BaseColumns {
    val ID = "id"
    val TABLE_NAME = "rack_table"
    val NAME_COL = "name"
    val CAPACITY_COL = "capacity"
    val POWER_CAPACITY_COL = "power_capacity"
    val ROOM_ID = "room_id"
}

object MachineTable: BaseColumns {
    val ID = "id"
    val TABLE_NAME = "machine_table"
    val POSITION_COL = "position"
    val RACK_ID = "rack_id"
}

object CPUTable: BaseColumns {
    val ID = "id"
    val TABLE_NAME = "cpu_table"
    val MANUFACTURER = "manufacturer"
    val FAMILY = "family"
    val GENERATION = "generation"
    val MODEL = "model"
    val CLOCK_RATE = "clock_rate"
    val CORES = "cores"
    val ENERGY_CONSUMPTION = "energy_consumption"
    val MACHINE_ID = "machine_id"
}

object GPUTable: BaseColumns {
    val ID = "id"
    val TABLE_NAME = "gpu_table"
    val MANUFACTURER = "manufacturer"
    val FAMILY = "family"
    val GENERATION = "generation"
    val MODEL = "model"
    val CLOCK_RATE = "clock_rate"
    val CORES = "cores"
    val ENERGY_CONSUMPTION = "energy_consumption"
    val MACHINE_ID = "machine_id"
}

