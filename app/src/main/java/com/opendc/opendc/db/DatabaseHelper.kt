package com.opendc.opendc.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.opendc.opendc.odcAndroid.jpa.schema.Datacenter

/**
 * @author: Zakarias Nordfäldt-Laws
 */

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME,
        null, DATABASE_VERSION) {

    private val SQL_DELETE_ALL_TABLES = "DROP TABLE IF EXISTS ${DatacenterTable.TABLE_NAME}, ${RoomTable.TABLE_NAME}, " +
            "${RackTable.TABLE_NAME}, ${MachineTable.TABLE_NAME}, ${CPUTable.TABLE_NAME}, ${GPUTable.TABLE_NAME}"

    private val CREATE_TABLE_DATACENTER = "CREATE TABLE ${DatacenterTable.TABLE_NAME} (" +
            "${DatacenterTable.ID} INTEGER PRIMARY KEY," +
            "${DatacenterTable.NAME_COL} TEXT" +
            ")"
    private val CREATE_TABLE_ROOM = "CREATE TABLE ${RoomTable.TABLE_NAME} (" +
            "${RoomTable.ID} INTEGER PRIMARY KEY," +
            "${RoomTable.NAME_COL} TEXT," +
            "${RoomTable.TYPE_COL} TEXT," +
            "${RoomTable.DATACENTER_ID} INTEGER," +
            "FOREIGN KEY(${RoomTable.DATACENTER_ID}) REFERENCES ${RackTable.TABLE_NAME}(${DatacenterTable.ID})" +
            ")"
    private val CREATE_TABLE_RACK = "CREATE TABLE ${RackTable.TABLE_NAME} (" +
            "${RackTable.ID} INTEGER PRIMARY KEY," +
            "${RackTable.NAME_COL} TEXT," +
            "${RackTable.CAPACITY_COL} INTEGER," +
            "${RackTable.POWER_CAPACITY_COL} REAL," +
            "${RackTable.ROOM_ID} INTEGER," +
            "FOREIGN KEY(${RackTable.ROOM_ID}) REFERENCES ${RoomTable.TABLE_NAME}(${RoomTable.ID})" +
            ")"
    private val CREATE_TABLE_MACHINE = "CREATE TABLE ${MachineTable.TABLE_NAME} (" +
            "${MachineTable.ID} INTEGER PRIMARY KEY," +
            "${MachineTable.POSITION_COL} INTEGER," +
            "${MachineTable.RACK_ID} INTEGER," +
            "FOREIGN KEY(${MachineTable.RACK_ID}) REFERENCES ${RackTable.TABLE_NAME}(${RackTable.ID})" +
            ")"
    private val CREATE_TABLE_CPU = "CREATE TABLE ${CPUTable.TABLE_NAME} (" +
            "${CPUTable.ID} INTEGER PRIMARY KEY," +
            "${CPUTable.MANUFACTURER} TEXT," +
            "${CPUTable.FAMILY} TEXT," +
            "${CPUTable.GENERATION} TEXT," +
            "${CPUTable.MODEL} TEXT," +
            "${CPUTable.CLOCK_RATE} INT," +
            "${CPUTable.CORES} INTEGER," +
            "${CPUTable.ENERGY_CONSUMPTION} REAL," +
            "${CPUTable.MACHINE_ID} INTEGER," +
            "FOREIGN KEY(${CPUTable.MACHINE_ID}) REFERENCES ${MachineTable.TABLE_NAME}(${MachineTable.ID})" +
            ")"
    private val CREATE_TABLE_GPU = "CREATE TABLE ${GPUTable.TABLE_NAME} (" +
            "${GPUTable.ID} INTEGER PRIMARY KEY," +
            "${GPUTable.MANUFACTURER} TEXT," +
            "${GPUTable.FAMILY} TEXT," +
            "${GPUTable.GENERATION} TEXT," +
            "${GPUTable.MODEL} TEXT," +
            "${GPUTable.CLOCK_RATE} INT," +
            "${GPUTable.CORES} INTEGER," +
            "${GPUTable.ENERGY_CONSUMPTION} REAL," +
            "${GPUTable.MACHINE_ID} INTEGER," +
            "FOREIGN KEY(${GPUTable.MACHINE_ID}) REFERENCES ${MachineTable.TABLE_NAME}(${MachineTable.ID})" +
            ")"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_DATACENTER)
        db.execSQL(CREATE_TABLE_ROOM)
        db.execSQL(CREATE_TABLE_RACK)
        db.execSQL(CREATE_TABLE_MACHINE)
        db.execSQL(CREATE_TABLE_CPU)
        db.execSQL(CREATE_TABLE_GPU)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ALL_TABLES)
        onCreate(db)
    }
}
