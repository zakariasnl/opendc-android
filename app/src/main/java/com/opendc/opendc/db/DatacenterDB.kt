package com.opendc.opendc.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.BitmapFactory
import com.opendc.opendc.odcAndroid.jpa.schema.*
import java.util.logging.Logger

/**
 * @author: Zakarias Nordfäldt-Laws
 */

class DatacenterDB(context: Context){
    private val dbHelper = DatabaseHelper(context)

    // Logging instance
    companion object {
        val LOG: Logger? = Logger.getLogger(DatacenterTable::class.java.name)
    }

    fun updateDatacenter(datacenter: Datacenter): Int {
        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put(DatacenterTable.NAME_COL, datacenter.name)

        val selection = "${DatacenterTable.ID}=${datacenter.id}"

        // TODO Update all sub tables

        return db.update(DatacenterTable.TABLE_NAME, values, selection, null)
    }

    fun storeDatacenter(datacenter: Datacenter): Long{

        val db = dbHelper.writableDatabase

        val values = ContentValues()
        values.put(DatacenterTable.NAME_COL, datacenter.name)

        val id = db.transaction{
            insert(DatacenterTable.TABLE_NAME, null, values)
        }

        datacenter.rooms.forEach {
            storeRoom(it, id)
        }

        return id
    }

    fun storeRoom(room: Room, datacenterId: Long): Long{
        val db = dbHelper.writableDatabase

        // Store room object
        val values = ContentValues()
        with (values) {
            put(RoomTable.NAME_COL, room.name)
            put(RoomTable.TYPE_COL, room.type.toString())
            put(RoomTable.DATACENTER_ID, datacenterId)
        }

        val id = db.transaction{
            insert(RoomTable.TABLE_NAME, null, values)
        }

        room.objects.forEach{
            storeRack(it as Rack, id)
        }

        return id
    }

    fun storeRack(rack: Rack, roomId: Long): Long{
        val db = dbHelper.writableDatabase

        // Store room object
        val values = ContentValues()
        with (values) {
            put(RackTable.NAME_COL, rack.name)
            put(RackTable.CAPACITY_COL, rack.capacity)
            put(RackTable.POWER_CAPACITY_COL, rack.powerCapacity)
            put(RackTable.ROOM_ID, roomId)
        }

        val id = db.transaction{
            insert(RackTable.TABLE_NAME, null, values)
        }

        rack.machines.forEach{
            storeMachine(it, id)
        }

        return id
    }

    fun storeMachine(machine: Machine, rackId: Long): Long{
        val db = dbHelper.writableDatabase

        // Store room object
        val values = ContentValues()
        with (values) {
            put(MachineTable.POSITION_COL, machine.position)
            put(MachineTable.RACK_ID, rackId)
        }

        val id = db.transaction{
            insert(MachineTable.TABLE_NAME, null, values)
        }

        machine.cpus.forEach{
            storeCPU(it, id)
        }

        machine.gpus.forEach{
            storeGPU(it, id)
        }

        return id
    }

    fun storeCPU(cpu: Cpu, idMachine: Long): Long{
        val db = dbHelper.writableDatabase

        // Store room object
        val values = ContentValues()
        with (values) {
            put(CPUTable.CLOCK_RATE, cpu.clockRate)
            put(CPUTable.CORES, cpu.cores)
            put(CPUTable.ENERGY_CONSUMPTION, cpu.energyConsumption)
            put(CPUTable.FAMILY, cpu.family)
            put(CPUTable.GENERATION, cpu.generation)
            put(CPUTable.MANUFACTURER, cpu.manufacturer)
            put(CPUTable.MODEL, cpu.model)
            put(CPUTable.MACHINE_ID, idMachine)
        }

        val id = db.transaction{
            insert(CPUTable.TABLE_NAME, null, values)
        }

        return id
    }

    fun storeGPU(gpu: Gpu, idMachine: Long): Long{
        val db = dbHelper.writableDatabase

        // Store room object
        val values = ContentValues()
        with (values) {
            put(GPUTable.CLOCK_RATE, gpu.clockRate)
            put(GPUTable.CORES, gpu.cores)
            put(GPUTable.ENERGY_CONSUMPTION, gpu.energyConsumption)
            put(GPUTable.FAMILY, gpu.family)
            put(GPUTable.GENERATION, gpu.generation)
            put(GPUTable.MANUFACTURER, gpu.manufacturer)
            put(GPUTable.MODEL, gpu.model)
            put(GPUTable.MACHINE_ID, idMachine)
        }

        val id = db.transaction{
            insert(GPUTable.TABLE_NAME, null, values)
        }

        return id
    }

    /**
     * Read all [datacenter] objects from the database
     *
     * @return ArrayList of all datacenters, sorted on ID
     */
    fun readAllDatacenters(): ArrayList<Datacenter> {
        val columns = arrayOf(DatacenterTable.ID, DatacenterTable.NAME_COL)

        val order = "${DatacenterTable.ID} ASC"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(DatacenterTable.TABLE_NAME, columns, orderBy=order)

        // Parse and store all datacenters
        val datacentersTmp = arrayListOf<Pair<Int, String>>()
        while (cursor.moveToNext()) {
            val id = cursor.getInt(DatacenterTable.ID)
            val name = cursor.getString(DatacenterTable.NAME_COL)
            datacentersTmp.add(Pair(id, name))
        }
        cursor.close()

        val datacenters = arrayListOf<Datacenter>()
        datacentersTmp.forEach{
            val rooms = readAllRooms(it.first)
            datacenters.add(Datacenter(it.first, it.second, rooms))
        }
        return datacenters
    }

    fun readDatacenter(idInput: Int): Datacenter{
        val columns = arrayOf(DatacenterTable.NAME_COL)

        val selection = "${DatacenterTable.ID}=$idInput"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(DatacenterTable.TABLE_NAME, columns, selection=selection)

        // Parse and store all datacenters
        val name: String
        if (cursor.moveToFirst()){
            name = cursor.getString(DatacenterTable.NAME_COL)
        } else {
            cursor.close()
            return Datacenter(idInput, "null", setOf())
        }

        cursor.close()

        val rooms = readAllRooms(idInput)
        return Datacenter(idInput, name, rooms)

    }

    fun readAllRooms(datacenterId: Int): Set<Room>{
        val columns = arrayOf(RoomTable.ID, RoomTable.NAME_COL, RoomTable.TYPE_COL)

        val order = "${RoomTable.ID} ASC"
        val selection = "${RoomTable.DATACENTER_ID}=${datacenterId}"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(RoomTable.TABLE_NAME, columns, selection=selection, orderBy=order)

        // Parse and store all Rooms
        val roomsTmp = mutableSetOf<Room>()
        while (cursor.moveToNext()) {
            val name = cursor.getString(RoomTable.NAME_COL)
            val id = cursor.getInt(RoomTable.ID)
            val roomType = getRoomType(cursor.getString(RoomTable.TYPE_COL))

            roomsTmp.add(Room(id, name, roomType, emptySet()))
        }
        cursor.close()

        val rooms = mutableSetOf<Room>()
        roomsTmp.forEach{
            val rack = readAllRacks(it.id)
            rooms.add(Room(it.id, it.name, it.type, rack))
        }

        return rooms
    }

    fun readAllRacks(roomId: Int): Set<Rack>{
        val columns = arrayOf(RackTable.ID, RackTable.NAME_COL, RackTable.POWER_CAPACITY_COL,
                RackTable.CAPACITY_COL)

        val order = "${RackTable.ID} ASC"
        val selection = "${RackTable.ROOM_ID}=${roomId}"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(RackTable.TABLE_NAME, columns, selection=selection, orderBy=order)

        // Parse and store all Rooms
        val racksTmp = mutableSetOf<Rack>()
        while (cursor.moveToNext()) {
            val id = cursor.getInt(RackTable.ID)
            val name = cursor.getString(RackTable.NAME_COL)
            val powerCapacity = cursor.getInt(RackTable.POWER_CAPACITY_COL)
            val capacity = cursor.getInt(RackTable.CAPACITY_COL)

            racksTmp.add(Rack(id, name, capacity, powerCapacity, emptyList()))
        }
        cursor.close()

        val racks = mutableSetOf<Rack>()
        racksTmp.forEach{
            val machines = readAllMachines(it.id)
            racks.add(Rack(it.id, it.name, it.capacity, it.powerCapacity, machines.toList()))
        }

        return racks
    }

    fun readAllMachines(rackId: Int): Set<Machine>{
        val columns = arrayOf(MachineTable.ID, MachineTable.POSITION_COL)

        val order = "${MachineTable.ID} ASC"
        val selection = "${MachineTable.RACK_ID}=${rackId}"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(MachineTable.TABLE_NAME, columns, selection=selection, orderBy=order)

        // Parse and store all Rooms
        val machineTmp = mutableSetOf<Machine>()
        while (cursor.moveToNext()) {
            val id = cursor.getInt(MachineTable.ID)
            val positionCol = cursor.getInt(MachineTable.POSITION_COL)

            machineTmp.add(Machine(id, positionCol, emptySet(), emptySet()))
        }
        cursor.close()

        val machines = mutableSetOf<Machine>()
        machineTmp.forEach{
            val cpu = readAllCpus(it.id)
            val gpu = readAllGpus(it.id)
            machines.add(Machine(it.id, it.position, cpu, gpu))
        }

        return machines
    }

    fun readAllCpus(machineId: Int): Set<Cpu> {
        val columns = arrayOf(CPUTable.ID, CPUTable.ENERGY_CONSUMPTION, CPUTable.CORES,
                CPUTable.CLOCK_RATE, CPUTable.FAMILY, CPUTable.MANUFACTURER,
                CPUTable.GENERATION, CPUTable.MODEL)

        val order = "${CPUTable.ID} ASC"
        val selection = "${CPUTable.MACHINE_ID}=${machineId}"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(CPUTable.TABLE_NAME, columns, selection=selection, orderBy=order)

        // Parse and store all Rooms
        val cpus = mutableSetOf<Cpu>()
        while (cursor.moveToNext()) {
            val id = cursor.getInt(CPUTable.ID)
            val manufacturer = cursor.getString(CPUTable.ID)
            val family = cursor.getString(CPUTable.ID)
            val generation = cursor.getString(CPUTable.ID)
            val model = cursor.getString(CPUTable.ID)
            val clockRate = cursor.getInt(CPUTable.ID)
            val cores = cursor.getInt(CPUTable.ID)
            val energyConsumption = cursor.getDouble(CPUTable.ENERGY_CONSUMPTION)

            cpus.add(Cpu(id, manufacturer, family, generation, model, clockRate, cores, energyConsumption))
        }
        cursor.close()

        return cpus
    }

    fun readAllGpus(machineId: Int): Set<Gpu> {
        val columns = arrayOf(GPUTable.ID, GPUTable.ENERGY_CONSUMPTION, GPUTable.CORES,
                GPUTable.CLOCK_RATE, GPUTable.FAMILY, GPUTable.MANUFACTURER,
                GPUTable.GENERATION, GPUTable.MODEL)

        val order = "${GPUTable.ID} ASC"
        val selection = "${GPUTable.MACHINE_ID}=${machineId}"

        val db = dbHelper.readableDatabase
        val cursor = db.doQuery(GPUTable.TABLE_NAME, columns, selection=selection, orderBy=order)

        // Parse and store all Rooms
        val gpus = mutableSetOf<Gpu>()
        while (cursor.moveToNext()) {
            val id = cursor.getInt(GPUTable.ID)
            val manufacturer = cursor.getString(GPUTable.ID)
            val family = cursor.getString(GPUTable.ID)
            val generation = cursor.getString(GPUTable.ID)
            val model = cursor.getString(GPUTable.ID)
            val clockRate = cursor.getInt(GPUTable.ID)
            val cores = cursor.getInt(GPUTable.ID)
            val energyConsumption = cursor.getDouble(GPUTable.ENERGY_CONSUMPTION)

            gpus.add(Gpu(id, manufacturer, family, generation, model, clockRate, cores, energyConsumption))
        }
        cursor.close()

        return gpus
    }

    fun getRoomType(input: String): RoomType{
        val roomType = when (input){
            "COOLING" -> RoomType.COOLING
            "HALLWAY" -> RoomType.HALLWAY
            "OFFICE" -> RoomType.OFFICE
            "POWER" -> RoomType.POWER
            "SERVER" -> RoomType.SERVER
            else -> throw Exception("RoomType ${input} is not valid")
        }

        return roomType
    }

    fun deleteDatacenter(datacenter: Datacenter): Int {
        val db = dbHelper.writableDatabase

        // Remove from datacenter table
        val selection = "${DatacenterTable.ID}=${datacenter.id}"

        datacenter.rooms.forEach {
            deleteRoom(it, datacenter.id)
        }

        return db.delete(DatacenterTable.TABLE_NAME, selection, null)
    }

    fun deleteRoom(room: Room, datacenterId: Int){
        val db = dbHelper.writableDatabase

        // Remove from room table
        val selection = "${RoomTable.DATACENTER_ID}=${datacenterId}"
        db.delete(RoomTable.TABLE_NAME, selection, null)

        // Delete all racks
        room.objects.forEach {
            deleteRack(it as Rack, room.id)
        }
    }

    fun deleteRack(rack: Rack, roomId: Int){
        val db = dbHelper.writableDatabase

        val selection = "${RackTable.ROOM_ID}=${roomId}"
        db.delete(RackTable.TABLE_NAME, selection, null)

        // Delete all machines
        rack.machines.forEach{
            deleteMachines(it, rack.id)
        }
    }

    fun deleteMachines(machine: Machine, rackId: Int){
        val db = dbHelper.writableDatabase

        var selection = "${MachineTable.RACK_ID}=${rackId}"
        db.delete(MachineTable.TABLE_NAME, selection, null)

        // Delete all cpus
        selection = "${CPUTable.MACHINE_ID}=${machine.id}"
        db.delete(CPUTable.TABLE_NAME, selection, null)

        // Delete all gpus
        selection = "${GPUTable.MACHINE_ID}=${machine.id}"
        db.delete(GPUTable.TABLE_NAME, selection, null)
    }
}

private fun SQLiteDatabase.doQuery(table: String, columns: Array<String>, selection: String? = null,
                                   selectionArgs: Array<String>? = null, groupBy: String? = null,
                                   having: String? = null, orderBy: String? = null) : Cursor {

    return query(table, columns, selection, selectionArgs, groupBy, having, orderBy)

}

private inline fun <T> SQLiteDatabase.transaction(function: SQLiteDatabase.() -> T): T {
    beginTransaction()
    val result = try {
        val returnValue = function()
        setTransactionSuccessful()

        returnValue
    } finally {
        endTransaction()
    }

    close()

    return result
}

private fun Cursor.getString(columnName: String) = getString(getColumnIndex(columnName))
private fun Cursor.getInt(columnName: String) = getInt(getColumnIndex(columnName))
private fun Cursor.getDouble(columnName: String) = getDouble(getColumnIndex(columnName))