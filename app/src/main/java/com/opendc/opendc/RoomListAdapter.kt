package com.opendc.opendc

/**
 * @author: Zakarias Nordfäldt-Laws
 */

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.TextView
import com.opendc.opendc.odcAndroid.jpa.schema.Room
import java.util.logging.Logger


/**
 * @author: Zakarias Nordfäldt-Laws
 */

class RoomListAdapter(private val context: Context,
                            private val dataSource: ArrayList<Room>) : BaseAdapter() {

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(RoomListAdapter::class.java.name)
    }

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.room_list_item, parent, false)

            holder = ViewHolder()
            holder.titleTextView = view.findViewById(R.id.listItemRoom) as TextView
            holder.deleteRoom = view.findViewById(R.id.deleteRoom) as ImageButton

            holder.deleteRoom.setOnClickListener {
                LOG?.info("Delete room at position ${position}")
                dataSource.removeAt(position)

                this.notifyDataSetChanged()
            }

            view.tag = holder
        }else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val titleTextView = holder.titleTextView
        val datacenterObject = getItem(position) as Room
        titleTextView.text = datacenterObject.name

        return view
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var deleteRoom: ImageButton
    }
}