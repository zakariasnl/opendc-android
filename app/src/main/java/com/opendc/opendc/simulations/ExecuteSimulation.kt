package com.opendc.opendc.simulations

import com.atlarge.opendc.omega.OmegaKernel
import java.util.concurrent.Executors
import com.atlarge.opendc.model.odc.platform.scheduler.Scheduler
import com.atlarge.opendc.simulator.kernel.Kernel
import com.opendc.opendc.experiments.JpaExperimentAndroid
import com.opendc.opendc.odcAndroid.jpa.schema.*
import java.util.logging.Logger

/**
 * @author: Zakarias Nordfäldt-Laws
 */


class ExecuteSimulation(private val timeout: Long = 30000L,
                        private val threads: Int = 4,
                        private val kernel: Kernel = OmegaKernel) {

    companion object {
        val LOG = Logger.getLogger(ExecuteSimulation::class.java.name)
    }

    private val executorService = Executors.newFixedThreadPool(threads)

    fun startSimulation(jpaExperiment: JpaExperimentAndroid) {
        LOG.info("Submitting experiment for simulation now..." )

        executorService.submit {
            jpaExperiment.use { it.run(kernel, timeout) }
        }

        Thread.sleep(500)
    }

    fun createExperiment(id: Int, name: String, scheduler: Scheduler, trace: Trace, path: Path
    ): Experiment {
        return Experiment(id, name, scheduler, trace, path)
    }

}
