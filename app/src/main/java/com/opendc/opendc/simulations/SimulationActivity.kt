package com.opendc.opendc.simulations

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.opendc.opendc.MainActivity
import com.opendc.opendc.R
import com.opendc.opendc.R.layout.activity_simulation
import com.opendc.opendc.datacenter.CreateExampleDatacenter
import com.opendc.opendc.db.DatacenterDB
import com.opendc.opendc.experiments.JpaExperimentAndroid
import com.opendc.opendc.odcAndroid.jpa.schema.*
import java.util.logging.Logger

/**
 * @author: Zakarias Nordfäldt-Laws
 */

class SimulationActivity(): AppCompatActivity() {
    private lateinit var datacenter: Datacenter
    lateinit var spinner: ProgressBar

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(SimulationActivity::class.java.name)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_simulation)

        val datacenterDB = DatacenterDB(this)

        // Check if we started inted by clicking on a certain database
        if (savedInstanceState == null){
            val extras = getIntent().extras
            if (extras != null){
                datacenter = datacenterDB.readDatacenter(extras.getInt(MainActivity.SELECTED_DATACENTER_WITH_ID))
            } else {
                // This should not happen, simulation started without any datacenter selected
                throw Exception("No datacenter found for simulation")
            }
        } else {
            datacenter = datacenterDB.readDatacenter(savedInstanceState.getSerializable(MainActivity.SELECTED_DATACENTER_WITH_ID) as Int)
        }

        // Initialize progress spinner
        spinner = findViewById(R.id.progressBarPerformingExperiment);
        spinner.visibility = View.VISIBLE
        // Get result text box to store everything in
        val resultTextBox: TextView = findViewById(R.id.simulationResultTextBox)

        val execution = ExecuteSimulation(timeout = 30000L)
        val datacenterFactory = CreateExampleDatacenter()

        val trace = datacenterFactory.createExampleTrace(datacenter.id, "trace0")
        val path = datacenterFactory.createExamplePath(datacenter.id, 1, datacenter)

        val experiment: Experiment = execution.createExperiment(datacenter.id, datacenter.name, datacenterFactory.scheduler,
                trace, path)

        experiment.state = ExperimentState.CLAIMED
        val jpaExperiment = JpaExperimentAndroid(experiment, resultTextBox, spinner, this)

        LOG?.info("Submitting simulation")

        SimulationVisualization().start(this, datacenter)

        execution.startSimulation(jpaExperiment)
    }
}