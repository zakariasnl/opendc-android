package com.opendc.opendc.simulations

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.util.TypedValue
import android.widget.LinearLayout
import android.widget.TextView
import com.opendc.opendc.R
import com.opendc.opendc.odcAndroid.jpa.schema.*
import java.util.logging.Logger


/**
 * @author: Zakarias Nordfäldt-Laws
 */

class SimulationVisualization {

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(SimulationActivity::class.java.name)
    }

    lateinit var activity: Activity

    fun start(_activity: Activity, datacenter: Datacenter){
        activity = _activity
        val scrollView: LinearLayout = activity.findViewById(R.id.simulationResultScrollBoxLinearLayout)
        val simulationResults = LinearLayout(activity)
        simulationResults.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        simulationResults.orientation = LinearLayout.VERTICAL

        datacenter.rooms.forEach {
            simulationResults.addView(addRoomUI(it))
        }

        scrollView.addView(simulationResults)
    }

    fun addRoomUI(room: Room): LinearLayout {
        val roomView = LinearLayout(activity)
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(0, 0, 0,20)
        roomView.layoutParams = layoutParams

        roomView.orientation = LinearLayout.VERTICAL
        roomView.setBackground(ContextCompat.getDrawable(activity, R.drawable.room_border))
        roomView.setPadding(10,10,30,20)

        val title = TextView(activity)
        title.text = room.name
        title.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        title.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark))
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simRoomTitleText))

        roomView.addView(title)

        room.objects.forEach {
            roomView.addView(addRackUI(it as Rack))
        }

        return roomView
    }

    fun addRackUI(rack: Rack): LinearLayout {
        val rackView = LinearLayout(activity)
        rackView.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        rackView.orientation = LinearLayout.VERTICAL

        val title = TextView(activity)
        title.text = rack.name
        title.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        title.setTextColor(ContextCompat.getColor(activity, R.color.colorAccent))
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simRackTitleText))

        rackView.addView(title)

        rack.machines.forEach{
            rackView.addView(addMachineUI(it))
        }

        return rackView
    }

    fun addMachineUI(machine: Machine): LinearLayout {
        val machineView = LinearLayout(activity)
        machineView.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        machineView.orientation = LinearLayout.VERTICAL
        machineView.id = machine.id


        val title = TextView(activity)
        title.text = "Machine ${machine.id.toString()}"
        title.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        title.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simMachineTitleText))

        machineView.addView(title)

        // Create LinearLayout wrapper for all values that will change during simulation
        val wrapper = LinearLayout(activity)
        wrapper.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        wrapper.orientation = LinearLayout.HORIZONTAL
        wrapper.setPadding(0,0,20,0)

        // Create wrapper for temp
        val tempWrapper = LinearLayout(activity)
        tempWrapper.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        tempWrapper.orientation = LinearLayout.VERTICAL

        val titleTemp = TextView(activity)
        titleTemp.text = "Temperature"
        titleTemp.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        titleTemp.setTextColor(ContextCompat.getColor(activity, R.color.black))
        titleTemp.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCPUTitleText))
        titleTemp.setPadding(0,0,10,0)

        val  valuesTemp = TextView(activity)
        valuesTemp.text = "0"
        valuesTemp.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        valuesTemp.setTextColor(ContextCompat.getColor(activity, R.color.black))
        valuesTemp.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCpuIndexTitleText))

        tempWrapper.addView(titleTemp)
        tempWrapper.addView(valuesTemp)

        // Create wrapper for memory
        val memWrapper = LinearLayout(activity)
        memWrapper.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        memWrapper.orientation = LinearLayout.VERTICAL

        val titleMem = TextView(activity)
        titleMem.text = "Memory"
        titleMem.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        titleMem.setTextColor(ContextCompat.getColor(activity, R.color.black))
        titleMem.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCPUTitleText))
        titleMem.setPadding(0,0,10,0)

        val  valuesMem = TextView(activity)
        valuesMem.text = "0"
        valuesMem.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        valuesMem.setTextColor(ContextCompat.getColor(activity, R.color.black))
        valuesMem.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCpuIndexTitleText))

        memWrapper.addView(titleMem)
        memWrapper.addView(valuesMem)


        // Create wrapper for load
        val loadWrapper = LinearLayout(activity)
        loadWrapper.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        loadWrapper.orientation = LinearLayout.VERTICAL

        val titleLoad = TextView(activity)
        titleLoad.text = "Load"
        titleLoad.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        titleLoad.setTextColor(ContextCompat.getColor(activity, R.color.black))
        titleLoad.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCPUTitleText))

        val  valuesLoad = TextView(activity)
        valuesLoad.text = "0"
        valuesLoad.layoutParams = (LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT))
        valuesLoad.setTextColor(ContextCompat.getColor(activity, R.color.black))
        valuesLoad.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.resources.getDimension(R.dimen.simCpuIndexTitleText))

        loadWrapper.addView(titleLoad)
        loadWrapper.addView(valuesLoad)

        wrapper.addView(tempWrapper)
        wrapper.addView(memWrapper)
        wrapper.addView(loadWrapper)

        machineView.addView(wrapper)

        return machineView
    }
}