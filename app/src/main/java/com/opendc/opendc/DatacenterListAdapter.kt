package com.opendc.opendc

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.TextView
import com.opendc.opendc.MainActivity.Companion.SELECTED_DATACENTER_WITH_ID
import com.opendc.opendc.datacenter.CreateDatacenter
import com.opendc.opendc.db.DatacenterDB
import com.opendc.opendc.odcAndroid.jpa.schema.Datacenter
import java.util.logging.Logger


/**
 * @author: Zakarias Nordfäldt-Laws
 */

class DatacenterListAdapter(private val context: Context,
                            private val dataSource: ArrayList<Datacenter>) : BaseAdapter() {


    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(DatacenterListAdapter::class.java.name)
    }


    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.datacenter_list_item, parent, false)

            holder = ViewHolder()
            holder.titleTextView = view.findViewById(R.id.list_item_name) as TextView
            holder.removeButton = view.findViewById(R.id.deleteDatacenter) as ImageButton
            holder.editButton = view.findViewById(R.id.editDatacenter) as ImageButton

            holder.removeButton.setOnClickListener {
                val datacenterDB = DatacenterDB(context)
                val datacenter = getItem(position) as Datacenter

                LOG?.info("Delete datacenter at with id: ${datacenter.id}")
                datacenterDB.deleteDatacenter(datacenterDB.readDatacenter(datacenter.id))

                dataSource.removeAt(position)

                this.notifyDataSetChanged()
            }

            holder.editButton.setOnClickListener{
                LOG?.info("Selected datacenter item: ${position}")

                val intent = Intent(it.context, CreateDatacenter::class.java)
                intent.putExtra(SELECTED_DATACENTER_WITH_ID, (getItem(position) as Datacenter).id)
                it.context.startActivity(intent)
            }

            view.tag = holder
        }else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val titleTextView = holder.titleTextView
        val datacenterObject = getItem(position) as Datacenter
        titleTextView.text = datacenterObject.name

        return view
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    private class ViewHolder {
        lateinit var titleTextView: TextView
        lateinit var editButton: ImageButton
        lateinit var removeButton: ImageButton
    }
}