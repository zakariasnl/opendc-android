package com.opendc.opendc.datacenter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import java.util.logging.Logger
import android.os.Bundle
import android.view.View
import android.widget.ListView
import com.opendc.opendc.MainActivity
import com.opendc.opendc.R
import com.opendc.opendc.R.layout.activity_add_datacenter
import com.opendc.opendc.RoomListAdapter
import com.opendc.opendc.db.DatacenterDB
import com.opendc.opendc.odcAndroid.jpa.schema.Datacenter
import com.opendc.opendc.odcAndroid.jpa.schema.Room
import kotlinx.android.synthetic.main.activity_add_datacenter.*


/**
 * @author: Zakarias Nordfäldt-Laws
 */

class CreateDatacenter : AppCompatActivity() {
    private lateinit var allDatacenters: MutableList<Datacenter>
    private lateinit var listView: ListView
    private lateinit var newDatacenter: Datacenter
    private var newId: Int = 0
    private var numberOfRooms = 0;
    private var updating = false

    private var rooms: ArrayList<Room> = arrayListOf()

    /**
     * Logging instance
     */
    companion object {
        val LOG: Logger? = Logger.getLogger(CreateDatacenter::class.java.name)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_add_datacenter)

        val datacenterDB = DatacenterDB(this)
        listView = findViewById(R.id.roomListView)

        // Check if we started inted by clicking on a certain database
        if (savedInstanceState == null){
            val extras = getIntent().extras
            if (extras != null){
                newDatacenter = datacenterDB.readDatacenter(
                        extras.getInt(MainActivity.SELECTED_DATACENTER_WITH_ID))
                newId = newDatacenter.id
                numberOfRooms = newDatacenter.rooms.size
                rooms.addAll(newDatacenter.rooms)
                updating = true
                datacenterTitleEditText.setText(newDatacenter.name)
            } else {
                setupNewDatabase(datacenterDB)
            }
        } else {
            newDatacenter = datacenterDB.readDatacenter(
                    savedInstanceState.getSerializable(MainActivity.SELECTED_DATACENTER_WITH_ID) as Int)
            newId = newDatacenter.id
            numberOfRooms = newDatacenter.rooms.size
            rooms.addAll(newDatacenter.rooms)
            updating = true
            datacenterTitleEditText.setText(newDatacenter.name)

        }

        // Add adapter
        val adapter = RoomListAdapter(this, rooms)
        listView.adapter = adapter

        // Add click listener for items
        listView.setOnItemClickListener { adapterView, view, position, id ->
            LOG?.info("Selected item: ${position}")
            // val selectedRoom = rooms[position]
        }

        // Add click listener for adding a new room
        addRoomButton.setOnClickListener {
            // TODO change here in order to make the user able to
            // TODO add his own room, instead of from fixed values
            addRoom()
        }
    }

    fun setupNewDatabase(datacenterDB: DatacenterDB){
        // TODO this should only read the last datacenter and not all
        allDatacenters = datacenterDB.readAllDatacenters()

        if (allDatacenters.size == 0){
            newId = 0
        }else {
            newId = allDatacenters[allDatacenters.size-1].id
        }

        rooms = arrayListOf()
    }

    fun addRoom(){
        LOG?.info("Adding a room")

        val datacenterFactory = CreateExampleDatacenter()
        rooms.addAll(datacenterFactory.createRooms("Server Room ${numberOfRooms++}"))

        val adapter = RoomListAdapter(this, rooms)
        listView.adapter = adapter
    }

    /**
     * Store created datacenter in SQLITE database and switch back to MainActivity
     */
    fun storeDatacenter(v: View){
        val title = datacenterTitleEditText.text.toString()

        // Check that there are at least 1 room and that there is a title
        if (title.length < 1){
            LOG?.info("Datacenter title to short")
            datacenterTitleEditText.selectAll()
            return
        } else if (rooms.size == 0) {
            LOG?.info("No rooms")
            return
        }


        newDatacenter = Datacenter(newId, title, rooms.toSet())

        val datacenterDB = DatacenterDB(v.context)

        if (updating){
            LOG?.info("Update datacenter with id: ${newDatacenter.id}")
            datacenterDB.updateDatacenter(newDatacenter)
        } else {
            LOG?.info("Save datacenter to database")
            datacenterDB.storeDatacenter(newDatacenter)
        }

        val intent = Intent(v.context, MainActivity::class.java)
        startActivity(intent)
    }
}