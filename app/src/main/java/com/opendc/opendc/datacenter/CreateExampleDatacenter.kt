package com.opendc.opendc.datacenter

import com.atlarge.opendc.model.odc.platform.scheduler.*
import com.opendc.opendc.odcAndroid.jpa.schema.*

/**
 * Create a simple "dummy" datacenter, containing fixed number of
 * racks, machines, jobs etc.
 *
 * @author: Zakarias Nordfäldt-Laws
 */

class CreateExampleDatacenter {
    var machineId = 0
    val scheduler: Scheduler

    init {
        scheduler = FifoScheduler()
    }

    fun createExampleDatacenter(id: Int, name: String): Datacenter {
        val rooms = createRooms("serverRoom0")

        return Datacenter(id, name, rooms)
    }

    fun createExampleTrace(id: Int, name: String): Trace {
        val jobs: MutableSet<Job> = mutableSetOf()

        jobs.add(Job(0, createTasks()))
        jobs.add(Job(1, createTasks()))

        return Trace(id, name, jobs)
    }

    fun createExamplePath(id: Int, numberOfSections: Int, datacenter: Datacenter): Path{
        val sections: MutableList<Section> = mutableListOf()

        (0..numberOfSections).forEachIndexed {index, it ->
            sections.add(Section(it, datacenter, 0L + index))
        }

        return Path(id, sections)
    }

    fun createTasks(): Set<Task>{
        val tasks: MutableSet<Task> = mutableSetOf()
        tasks.add(Task(0, 1000, null, false, 0L))
        tasks.add(Task(1, 6000, null, false, 10L))
        tasks.add(Task(2, 300, null, false, 20L))
        tasks.add(Task(3, 80000, null, false, 100L))
        tasks.add(Task(4, 5800, null, false, 40L))

        return tasks
    }

    fun createRooms(name: String): Set<Room> {
        val rooms: MutableSet<Room> = mutableSetOf()

        rooms.add(Room(0, name, RoomType.SERVER, setOf(
                Rack(0, "rack0", 1,12, createMachines()),
                Rack(1, "rack1", 2,15, createMachines2())
        )))


        return rooms
    }

    fun createMachines(): List<Machine> {
        val machines: MutableList<Machine> = mutableListOf()
        val cpu = Cpu(0, "manufacturer", "family", "generation",
                "model", 34, 4, 40.5)
        val gpu = Gpu(0, "manufacturer", "family", "generation",
                "model", 300, 6000,19.7)

        machines.add(Machine(machineId++, 0, setOf(cpu), setOf(gpu)))
        //machines.add(Machine(machineId++, 1, setOf(cpu), setOf(gpu)))

        return machines
    }

    fun createMachines2(): List<Machine> {
        val machines: MutableList<Machine> = mutableListOf()
        val cpu = Cpu(0, "manufacturer", "family", "generation",
                "model", 23, 2, 8.4)
        val gpu = Gpu(0, "manufacturer", "family", "generation",
                "model", 479, 2500,5.3)

        machines.add(Machine(machineId++, 0, setOf(cpu), setOf(gpu)))
        machines.add(Machine(machineId++, 0, setOf(cpu), setOf(gpu)))

        return machines
    }
}
